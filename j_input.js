function getDate() {
      const monthArray = ["Jan ", "Feb ", "Mar ", "Apr ", "May ", "Jun ", "Jul ", "Aug ", "Sep ", "Oct ", "Nov ", "Dec "];
      const dayArray = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"]

      const date = new Date();
      document.getElementById("date").innerHTML = date.getUTCDate() - 1;
      document.getElementById("month").innerHTML = monthArray[date.getUTCMonth()];
      document.getElementById("year").innerHTML = date.getUTCFullYear();
      document.getElementById("day").innerHTML = dayArray[date.getUTCDay()];

}

function getResult(){
    var formData = new FormData(document.getElementById("initForm"));

    document.getElementById("test").innerHTML = formData.getAll("birthday");
}